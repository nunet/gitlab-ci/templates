# Slack Notification

This template implements a job template that can be installed in existing jobs to add notification capabilities.

# Pre-requisites

The job extending the slack notification template job needs to have:

- curl
- bash

Bash is needed for capitalization substitution (`${...^^}`), which doesn't work on `ash` for instance.

# Enrivonment variables

| Name                                 | Description                                                                                                                     | Type     | Default | Example                                                                         |
|--------------------------------------|---------------------------------------------------------------------------------------------------------------------------------|----------|---------|---------------------------------------------------------------------------------|
| `SLACK_WEBHOOK`                      | Must contain a valid  [Slack API incoming webhook](https://api.slack.com/messaging/webhooks)  URL used to send the notification | `string` | N/A     | https://hooks.slack.com/services/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 |
| `SLACK_NOTIFY_FAILURE_ONLY`          | Controls whether to send notifications only on failed executions                                                                | `bool`   | `false` | `true`, `false`, `1`, `0`                                                       |
| `SLACK_DISABLE_SUCCESS_NOTIFICATION` | Controls whether to disable notification of successful executions                                                               | `bool`   | `false` | `true`, `false`, `1`, `0`                                                       |

# Usage

Include this template into your `.gitlab-ci.yml` file and extend `.slack_notification`:

```yaml
include:
  - project: "nunet/gitlab-ci/templates"
    ref: main
    file: "slack-notification/Slack-Notification.gitlab-ci.yml"

job_to_notify:
  # ...
  extends:
    - .slack_notification
```

The template job uses `after_script` to handle the logic for sending webhooks. If your job already has an `after_script` block, you can still use it referencing
the template job `after_script` block directly:

```yaml
job_with_after_script:
  # ...
  extends:
    - .slack_notification
  after_script:
    - # some bash after script
    - !reference [.slack_notification, after_script]
```

For actual examples see this [template tests](slack-notification/tests/Tests.gitlab-ci.yml).

