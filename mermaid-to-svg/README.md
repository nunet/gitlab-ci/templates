# Mermaid to SVG

This gitlab ci template uses mermaid cli to generate SVG files and commit to the source repository.

# Pre-requisites

It needs a project scoped access token with read and write access to the project's repository.

The variable the template expects the token to be in is called `PROJECT_CI_COMMIT_ACCESS_TOKEN`.

# Usage

Include this template into your `.gitlab-ci.yml` file:

```yaml
include:
  - project: "nunet/gitlab-ci/templates"
    ref: main
    file: "mermaid-to-svg/Generate-SVGs.gitlab-ci.yml"
```

Let's say you already have a git read/write token for the project named `GITLAB_ACCESS_TOKEN` and you want to use that as is, you can pass this variable to the mermaid render job:

```yaml
include:
  - project: "nunet/gitlab-ci/templates"
    ref: main
    file: "mermaid-to-svg/Generate-SVGs.gitlab-ci.yml"

mermaid_to_svg:
  variables:
    PROJECT_CI_COMMIT_ACCESS_TOKEN: $GITLAB_ACCESS_TOKEN
```

# What does it do

It uses chromium headless to spawn a process that renderers the mermaid SVG. It uses puppeteer to interact with the browser.
Calls are managed by the mermaid cli `mmdc`.

All the necessary dependencies are stored in the docker image corresponding to this template.
