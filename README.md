# templates

This repository holds gitlab ci templates that are useful for other projects and are generic enough so not be hosted in these other projects

# Current templates

The following is a list of templates currently implemented.

For a more in depth documentation refer to each individual template folder.

## mermaid to svg

This template searches for `BASENAME.mermaid` files and renders them into `rendered/BASENAME.svg` using mermaid `mmdc` cli, puppeteer and chromium headless.

## docker

This template has a job template that can be extended to compile and push docker images to gitlab registry.
