# Docker

This folder holds templates related to docker functionalities for the Gitlab CI.

# Docker Build

This template hosts a job template that is supposed to be extended with the necessary parameters for image build.
It builds a docker image using [buildah](https://buildah.io/) and pushes it to the Gitlab CI container registry.

## Usage

You need to extend the job template with the necessary information about the docker image to be built.
Let's use the following folder structure as an example:

```
 project-root
├──  subpath
│   └──  image-name
│       ├──  Dockerfile.prod
└──  README.md
```

To build the docker image using `Dockerfile.prod` inside `image-name` folder:

```yaml
include:
  - project: "nunet/gitlab-ci/templates"
    ref: main
    file: "docker/Docker-Build.gitlab-ci.yml"

docker_build_and_push: # change this job name as needed
  extends: .docker_build  
  variables:
    IMAGE_NAME: image-name
    IMAGE_PATH: ./path/to/image
    DOCKERFILE_NAME: Dockerfile.prod
```

This will build and push a docker image with the name `image-name` to the project's Gitlab Container registry.

